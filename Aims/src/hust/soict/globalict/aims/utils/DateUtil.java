package hust.soict.globalict.aims.utils;

public class DateUtil {
	public static int compare2(MyDate date1, MyDate date2) {
		// 1 before ; -1 means after
		if(date1.getYear() < date2.getYear()) {
			return 1;
		} else if (date1.getYear() > date2.getYear()) {
			return -1;
		} else {
			// compare month
			if(date1.getMonth() < date2.getMonth()) {
				return 1;
			} else if (date1.getMonth() > date2.getMonth()) {
				return -1;
			} else {
//				compare date
				if(date1.getDay()<date2.getDay()) {
					return 1;
				} else if(date1.getDay() > date2.getDay()){
					return -1;
				} else {
					return 0;
//					System.out.println("Two dates are the same.");
				}
			}
		}
	}
	
	public static void compareResult(MyDate date1, MyDate date2) {
		if(compare2(date1, date2) == 1) {
			System.out.println(date1.day+"/"+date1.month + "/"+date1.year + " is before "+ 
					date2.day+"/"+date2.month + "/"+date2.year);
		} else if(compare2(date1, date2) == -1) {
			System.out.println(date1.day+"/"+date1.month + "/"+date1.year + " is after "+ 
					date2.day+"/"+date2.month + "/"+date2.year);
		} else {
			System.out.println("Two dates are the same.");			
		}
	}
	public static void swapDate(MyDate date1, MyDate date2) {
		MyDate tmpDate = new MyDate();
		tmpDate.setDate(date1.getDay(), date1.getMonth(), date1.getYear());
		date1.setDate(date2.getDay(), date2.getMonth(), date2.getYear());
		date2.setDate(tmpDate.getDay(), tmpDate.getMonth(), tmpDate.getYear());			
	}
	
	public static void sortDate(MyDate ...dates) {
		for(int i=0; i< dates.length - 1; i++) {
			for(int j=i+1; j< dates.length; j++) {
				if(compare2(dates[i], dates[j]) == 1) {
					swapDate(dates[i], dates[j]);
				}
			}
		}
		for(int i=0; i< dates.length; i++) {
			dates[i].print();
		}
	}
	
	
}
