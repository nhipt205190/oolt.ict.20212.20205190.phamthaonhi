package hust.soict.globalict.aims.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MyDate {
	int day;
	int month;
	int year;
	String strday;
	String strmonth;
	String stryear;
	LocalDateTime today;
	
	String monthList[] = {"January","February","March","April", "May","June","July","August",
            "September",
            "October",
            "November",
            "December"};
	
	public MyDate(){
		today = LocalDateTime.now();
//		System.out.println(dtf.format(now));        //  2021/03/22 16:37:15
		this.day = today.getDayOfMonth();
		this.month = today.getMonthValue();
		this.year = today.getYear();		
	}

	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(String date) {
		accept(date);
	}
	public MyDate(String day, String month, String year) {
		this.strday= day;
		this.strmonth = month;
		this.stryear = year;
	}
	
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	public void accept(String inputDate) {
		int i=0;
		String searchFirstSpace = " ";
		i = inputDate.indexOf(searchFirstSpace);
		for(int k=0; k < this.monthList.length; k++) {
			if(this.monthList[k].equals(inputDate.substring(0,i))){
				setMonth(k+1);
				break;
			}
		}
		setDay(Integer.parseInt(inputDate.substring(i+1, i+3)));
		setYear(Integer.parseInt(inputDate.substring(i+4, i+8)));	
	}
	public void accept() {
		System.out.print("Enter date (eg: \"March 03 2002\"): ");
		Scanner sc = new Scanner(System.in);
		String inputDate = sc.nextLine();
		sc.close();
		

		int i=0;
		String searchFirstSpace = " ";
		i = inputDate.indexOf(searchFirstSpace);
		for(int k=0; k < this.monthList.length; k++) {
			if(this.monthList[k].equals(inputDate.substring(0,i))){
				setMonth(k+1);
				break;
			}
		}
		setDay(Integer.parseInt(inputDate.substring(i+1, i+3)));
		setYear(Integer.parseInt(inputDate.substring(i+4, i+8)));		
	}
	public void printN() {
		System.out.println(this.strmonth + " " + this.strday + " " + this.stryear);
	}
	public void print() {
		System.out.println(this.day + "/"+this.month + "/" + this.year);
	}
	public void printDateTimeFormat() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu/MM/dd HH:mm:ss");
		System.out.println(dtf.format(this.today));
	}
	
	public void setDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
}
