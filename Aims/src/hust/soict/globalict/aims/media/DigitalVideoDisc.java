package hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Disc implements Playable {
//	private String director;
	private int length;
	
	public DigitalVideoDisc(String title, String category, float cost, int id, String director, int length) {
		super(title, category, cost, id, director, length);
		// TODO Auto-generated constructor stub
	}
	public DigitalVideoDisc(String title, String category, float cost, int id) {
		super(title, category, cost, id);
		// TODO Auto-generated constructor stub
	}
	public DigitalVideoDisc(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}
	
	public DigitalVideoDisc(String title, String category, float cost, int id, String director, int length,
			int length2) {
		super(title, category, cost, id, director, length);
		length = length2;
	}
	
	
	
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public boolean search(String title) {
		title = title.toLowerCase();
		String originalTitle = getTitle().toLowerCase();
		String[] splittedWord = title.split(" ");
		for(int i=0; i< splittedWord.length; i++) {
			if(originalTitle.contains(splittedWord[i]) == false) {
				return false;
			}
		}
		return true;
	}
	@Override
//	public void print() {
//		System.out.println("Item ID: " + getId());
//		System.out.println("\t.DVD - " + getTitle() + " - " + getCategory() + " - " + getCost() + " - Director: "+ getDirector()+ " - Length: " + getLength());
//	}
	public void play() {
		System.out.println("\tPlaying DVD: " + this.getTitle());
		System.out.println("\tDVD length: " + this.getLength());
	}
	
	@Override
	public String toString() {
		return this.getId() + " .DVD - "+ this.getTitle()+" - " +this.getCategory()+" - "+this.getDirector()+" - "+this.getLength()+" - "+this.getCost();
	}
}
