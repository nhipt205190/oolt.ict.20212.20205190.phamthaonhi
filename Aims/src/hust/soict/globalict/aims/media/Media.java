package hust.soict.globalict.aims.media;

import java.util.Comparator;

//import java.util.Comparator;

public abstract class Media implements Comparable<Media> {
	private String title;
	private String category;
	private float cost;
	private int id;
	
	public static final Comparator<Media> COMPARE_BY_TITLE_COST = new MediaComparatorByTitleCost();
	public static final Comparator<Media> COMPARE_BY_COST_TITLE = new MediaComparatorByCostTitle();
	
	
public Media(String title, String category, float cost, int id) {
		super();
		this.title = title;
		this.category = category;
		this.cost = cost;
		this.id = id;
	}

public Media(String title, String category, float cost) {
	super();
	this.title = title;
	this.category = category;
	this.cost = cost;
}

	public String getTitle() {
		return title;
	}

	public String getCategory() {
		return category;
	}

	public float getCost() {
		return cost;
	}

	public int getId() {
		return id;
	}

//	public void print() {};
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof Media) {
			Media objMedia = (Media)o;
			if(this.getId() == objMedia.id) {
				return true;
			}else {
				return false;
			}
		} else {
			return false;
		}		
	}
	@Override
	public int compareTo(Media o) {
		if(this.getTitle().compareTo(o.getTitle())==0) {
			return this.getCategory().compareTo(o.getCategory());
		} else {
			return this.getTitle().compareTo(o.getTitle());
		}
	}
}
