package hust.soict.globalict.aims.media;

public class Track implements Playable{
	private String title;
	private int length;
	
	public Track(String title, int length) {
		super();
		this.title = title;
		this.length = length;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getTitle() {
		return title;
	}
	public int getLength() {
		return length;
	}
	public void play() {
		System.out.println("\tPlaying track: " + this.getTitle());
		System.out.println("\t\tTrack's length: " + this.getLength());
		}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof Track) {
			Track obTrack = (Track)o;
			if(obTrack.length == this.getLength() && obTrack.title.equals(this.getTitle())) {
				return true;
			} else return false;
		} else return false;
	}
	@Override
	public String toString() {
		return this.getTitle() + "-" + this.getLength() + "s";
	}
}
