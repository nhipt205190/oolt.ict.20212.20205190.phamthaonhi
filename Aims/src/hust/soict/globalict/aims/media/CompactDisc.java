package hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable{
	private String artist;
	private ArrayList<Track> tracks = new ArrayList<>();
	
//constructors
	public CompactDisc(String title, String category, float cost, int id, String director, int length) {
		super(title, category, cost, id, director, length);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title, String category, float cost, int id) {
		super(title, category, cost, id);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title, String category, float cost, int id, String director, int length, String artist,
			ArrayList<Track> tracks) {
		super(title, category, cost, id, director, length);
		this.artist = artist;
		this.tracks = tracks;
	}

	public String getArtist() {
		return artist;
	}
	
	public void setArtist(String artist) {
		this.artist = artist;
	}

	public void addTrack(Track inputTrack) {
		if(tracks.contains(inputTrack) == false) {
			tracks.add(inputTrack);
			System.out.println("\t>>> ADD TRACK SUCCESSFULLY >>>");
		} else {
			System.out.println("\t>>> THE TRACK HAS ALREADY BEEN IN THE LIST");
		}
	}
	public void removeTrack(Track inputTrack) {
		if(tracks.contains(inputTrack) == true) {
			tracks.remove(tracks.indexOf(inputTrack));
		} else {
			System.out.println(">>> ERROR. THE TRACK IS NOT IN THE LIST >>>");
		}
	}
	@Override
	public int getLength() {
		int totalLength = 0;
		for(int i =0; i < tracks.size(); i++) {
			totalLength = totalLength + tracks.get(i).getLength();
		}
		return totalLength;
	}
//	@Override
//	public void print() {
//		System.out.println("Item ID: " + getId());
//		System.out.println("\t.CD - " + this.getTitle() + " - " + this.getCategory() + " - " + this.getCost() + " - Artist: "+ getArtist()
//														+ " - Length: " + getLength());
//	}
	public void play() {
		System.out.println("\tPlaying CD: " + this.getTitle());
		System.out.println("\tCD length: " + this.getLength());
		System.out.println("\tList of tracks: ");
		for(int i =0; i< tracks.size(); i++) {
			System.out.printf("\t%d. ", i+1);
			tracks.get(i).play();
		}
	}
	
	@Override
	public String toString() {
		return this.getId()	+ " .CD - " +this.getTitle()+"-" +this.getCategory()+"-" +this.getArtist()+"-"+
						this.getDirector()+"-" +this.getLength()+"-" +this.getCost()+"-" +this.tracks;
	}
}
