package hust.soict.globalict.aims.media;

public class Disc extends Media {
	private String director;
	private int length;
	
	
public Disc(String title, String category, float cost, int id, String director, int length) {
		super(title, category, cost, id);
		this.director = director;
		this.length = length;
	}
//	public Disc() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//
//	public Disc(String title) {
//		super(title);
//		// TODO Auto-generated constructor stub
//	}
//
//	public Disc(String title, String category) {
//		super(title, category);
//		// TODO Auto-generated constructor stub
//	}
//
//	public Disc(String title, String category, float cost) {
//		super(title, category, cost);
//		// TODO Auto-generated constructor stub
//	}
//
//	public Disc(String title, String category, float cost, String director, int length) {
//		super(title, category, cost);
//		this.director = director;
//		this.length = length;
//	}
//
//	public Disc(String title, String category, float cost, int id) {
//		super(title, category, cost, id);
//		// TODO Auto-generated constructor stub
//	}

	public Disc(String title, String category, float cost, int id) {
		super(title, category, cost, id);
		// TODO Auto-generated constructor stub
	}
	public Disc(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}
	public String getDirector() {
		return director;
	}	
	public void setDirector(String director) {
		this.director = director;
	}

	public int getLength() {
		return length;
	}
}
