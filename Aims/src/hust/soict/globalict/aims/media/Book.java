package hust.soict.globalict.aims.media;

import java.util.ArrayList;
//import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Book extends Media {
	private List<String> authors = new ArrayList<String>();
	private String content = new String();
	private int lengthBook;
	List<String> contentTokens = new ArrayList<String>();
	Map<String, Integer> wordFrequency;
	
	//comparator class
	public static class CustomComparator implements Comparator<String>{

		@Override
		public int compare(String o1, String o2) {
			int value =  o1.compareTo(o2);

            // elements are sorted in reverse order
            if (value > 0) {
                return -1;
            }
            else if (value < 0) {
                return 1;
            }
            else {
                return 0;
            }
		}
		
	}
	public void processContent() {
		String[] splittedWord = content.split(" ");
		for(int i=0; i< splittedWord.length; i++) {
			contentTokens.add(splittedWord[i]);			
		}
		//store the length
		this.lengthBook = splittedWord.length;
		//content Tokens before sorted
		Iterator<String> iterator = contentTokens.iterator();
	    System.out.println(">>>Current content tokens are: ");
	    while(iterator.hasNext()) {
	    	System.out.println(iterator.next());
	    }
	    System.out.println("--------------------------------------------");
		Collections.sort(contentTokens);
		iterator = contentTokens.iterator();
	    System.out.println(">>>Sorted content tokens are: ");
	    while(iterator.hasNext()) {
	    	System.out.println(iterator.next());
	    }
	    System.out.println("--------------------------------------------");
		
		//create tree map , sort frequency
		TreeMap<String, Integer> frequencyMap = new TreeMap<>();
		for(String s:contentTokens) {
			Integer count = frequencyMap.get(s);
			if(count == null) { count = 0;}
			frequencyMap.put(s, count+1);
		}
		for(Map.Entry<String, Integer> entry: frequencyMap.entrySet()) {
			System.out.println("'" + entry.getKey()+"'" + ": " + entry.getValue());
		}
		System.out.println("--------------------------------------------");
		System.out.println("Length of book is: " + this.getLengthBook());
	}
	
public Book(String title, String category, float cost, int id, List<String> authors, String content) {
		super(title, category, cost, id);
		this.authors = authors;
		this.content = content;
	}

	public Book(String title, String category, float cost, List<String> authors) {
		super(title, category, cost);
		this.authors = authors;
		// TODO Auto-generated constructor stub
	}
	
	public Book(String title, String category, float cost, int id) {
		super(title, category, cost, id);
		// TODO Auto-generated constructor stub
	}

	public Book(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}

	public void setContent(String content) {
		this.content = content;
	}
	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	public int getLengthBook() {
		return lengthBook;
	}

	public void addAuthor(String authorName) {
		if(authors.contains(authorName) == false) {
			authors.add(authorName);
		}
	}
	public void removeAuthor(String authorName) {
		if(authors.contains(authorName) == true) {
			authors.remove(authors.indexOf(authorName));			
		}
	}
//	@Override
//	public void print() {
//		System.out.println("Item ID: " + getId());
//		System.out.println("\t.BOOK - " + getTitle() + " - " + getCategory() + " - " + getCost() + 
//				" - Authors: "+ authors);
//	}
	@Override
	public String toString() {
		return this.getId() + " .Book - " + this.getTitle() +"-" + this.getCategory() +"-" + this.authors + "-"+ this.getLengthBook();
		//thieu content length
	}
}
