package hust.soict.globalict.aims;
import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.order.Order;

public class Aims extends Thread{
	@Override
	public void run() {
		actions();
	}
	public static void showMenu() {
		System.out.println("\nOrder Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}	

	public void actions(){		
		ArrayList<Order> Orders = new ArrayList<Order>();
		int currentID=-1;
		while(true) {
			showMenu();
			Scanner s = new Scanner(System.in);
			int choice = s.nextInt();
			switch (choice) {
				case 0:
					System.exit(0);
				case 1:
					Order anOrder = new Order();
					Orders.add(anOrder);
					currentID = anOrder.getId();
					break;
				case 2:
					System.out.println("\t>>> ADD AN ITEM >>>");
					System.out.println("Which kind of item do you want?");
					System.out.println("1. Book\n2. DigitalVideoDisc\n3. CompactDisc");
					System.out.print("Your choice: ");
					choice = s.nextInt();
					s.nextLine();

					int id;
					String tmpTitle = new String();
					String tmpCatergory;
					float tmpCost;
					System.out.print("\n\tItem ID: ");
					id = s.nextInt();
					s.nextLine();
					//check the ID here
				
					System.out.print("\n\tTitle:");
					tmpTitle = s.nextLine();
					
					System.out.print("\tCategory: ");
					tmpCatergory = s.nextLine();
					
					System.out.print("\tCost: ");
					tmpCost = s.nextFloat();
					s.nextLine();
					
					Media newItem;
					
					if(choice == 1) {
						newItem = new Book(tmpTitle,tmpCatergory,tmpCost,id); //Upcast Book to Media
						Book b1 = (Book)newItem; //Down-casting
						
						do {
							System.out.print("\tAuthors: ");
							b1.addAuthor(s.nextLine());
							
							System.out.println("Do you want to add more author?");
							System.out.println("1. Yes\n2. No");
							System.out.print("Your choice: ");
							choice = s.nextInt();
							s.nextLine();
							
							if(choice == 2) {
								break;
							}
						} while(choice == 1);
						
					}
					else if(choice == 2) {
						newItem = new DigitalVideoDisc(tmpTitle,tmpCatergory,tmpCost,id);
						
						DigitalVideoDisc dvd = (DigitalVideoDisc)newItem; //Down-casting
						System.out.print("\tDirector: ");
						dvd.setDirector(s.nextLine());
						
						System.out.print("\tLength: ");
						dvd.setLength(s.nextInt());
						s.nextLine();
						
//						PLAYITEM DVDs
						System.out.println("\n***Do you want to play?");
						System.out.println("***1. Yes\n***2. No");
						System.out.print("***Your choice: ");
						choice = s.nextInt();
//						s.nextInt();
						
						if(choice == 1) {
							System.out.println("\n\t>>> START PLAYING >>>\n");
							dvd.play();
							System.out.println("\n\t>>> COMPLETE PLAYING>>>");
						}	
						
					}
					else if(choice == 3) {
						newItem = new CompactDisc(tmpTitle,tmpCatergory,tmpCost,id);
						CompactDisc cd = (CompactDisc)newItem;
						System.out.print("\tArtist: ");
						cd.setArtist(s.nextLine());
						
						int i=1;
						System.out.println("\n\t>>> LIST OF TRACKS >>>");
						String tmpTitleTrack;
						int tmpLengthTrack;

						do {
							
							System.out.printf("\n\tNo.%d \n", i);
							System.out.print("\tTitle: ");
							tmpTitleTrack = s.nextLine();
							
							System.out.print("\tLength: ");
							tmpLengthTrack = s.nextInt();
							
							Track tmpTrack = new Track(tmpTitleTrack, tmpLengthTrack);
							cd.addTrack(tmpTrack);
							i++;
							
							System.out.println("\nDo you want to add more track?");
							System.out.println("1. Yes\n2. No");
							System.out.print("Your choice: ");
							choice = s.nextInt();
							s.nextLine();
							
							if(choice == 2) {
								break;
							}
						}while(choice == 1);

//						PLAY ITEM CDs
						System.out.println("\n***Do you want to play?");
						System.out.println("***1. Yes\n***2. No");
						System.out.print("***Your choice: ");
						choice = s.nextInt();
//						s.nextInt();
						
						if(choice == 1) {
							System.out.println("\n\t>>> START PLAYING >>>");
							cd.play();
							System.out.println("\n\t>>> COMPLETE PLAYING>>>");
						}	
						
					}
					else {
						System.out.println("\tWrong type!");
						break;
					}
					//add media item to array of orders
					Orders.get(currentID-1).addMedia(newItem);	
					break;
				case 3:
					System.out.println("\t>>>DELETE ITEM BY ID>>>");
					int searchID;
					System.out.print("\tEnter ID: ");
					searchID = s.nextInt();
					int flag =0;
					for(int i=0; i<Orders.size(); i++) {
						if(Orders.get(i).getId() == searchID) {
							Orders.remove(i);
							flag = 1;
							break;
						}
					}
					if(flag == 1) {
						System.out.println("\t...DELETE SUCCESSFULLY...");
					} else {
						System.out.println("\t...CANNOT FIND ID...");
					}
					break;
				case 4:
					for(int i=0; i< Orders.size(); i++) {
						Orders.get(i).printListOfItem();
					}
					break;
				default:
					s.close();
					System.exit(0);
			}
		}
	}
	
	public static void main(String[] args) {
		Aims aimsObj = new Aims();
		aimsObj.setName("Aims' thread");
		
		MemoryDaemon md1 = new MemoryDaemon();
		Thread t1 = new Thread(md1);
		t1.setDaemon(true);
		
		aimsObj.start();
		t1.start();
		try {
			t1.join(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
}
