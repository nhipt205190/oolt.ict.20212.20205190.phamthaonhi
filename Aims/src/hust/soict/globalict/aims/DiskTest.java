package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.DigitalVideoDisc;

public class DiskTest {

	public static void main(String[] args) {
		DigitalVideoDisc ob1 = new DigitalVideoDisc("PotterHarry and the philosopher's stone","PE",19.99f,1,"JKRowling",90);
		System.out.println(ob1.search("hArry  Potter"));
	}

}
