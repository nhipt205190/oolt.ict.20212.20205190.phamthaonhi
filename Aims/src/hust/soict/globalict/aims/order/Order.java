package hust.soict.globalict.aims.order;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;
import java.util.ArrayList;
public class Order {
	private MyDate dateOrdered;
	private ArrayList<Media> itemsOrdered = new ArrayList<>();
	private int id;
	private static int nbOrders = 0;
	
	public Order() {
			this.dateOrdered = new MyDate();
			setNbOrders();
			setId();		
	}
	
	public int getId() {
		return id;
	}

	public void setId() {
		id = getNbOrders();
	}

	public static int getNbOrders() {
		return nbOrders;
	}

	public static void setNbOrders() {
		Order.nbOrders++;
	}

	public MyDate getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public void addMedia(Media inputItem) {
		if(itemsOrdered.contains(inputItem) == false) {
			itemsOrdered.add(inputItem);
			System.out.println("\t>>> ADD MEDIA ITEM SUCCESSFULLY >>>");
		} else {
			System.out.println("\n\t>>> CANNOT ADD THIS ITEM SINCE IT HAS SAME ID WITH THOSE IN THE LIST >>>");
		}
		
	}
	
	public void removeMedia(Media item) {
		if( itemsOrdered.remove(item) == true) {
			System.out.println("Item is successfully removed");
		} else{
			System.out.println("Item is not found");
		}
	}
	
	public float totalCost() {
		float sum=0.00f;
		for(int i =0; i<itemsOrdered.size(); i++) {
			sum = sum + itemsOrdered.get(i).getCost();
		}
		return sum;
	}
	
	public void printListOfItem() {
		System.out.println("***********************Order***********************");
		System.out.println("Order ID: " + getId());
		System.out.print("Date: ");
		dateOrdered.printDateTimeFormat();
		System.out.println("Ordered Items: ");
		for(int i=0; i<itemsOrdered.size(); i++) {
			System.out.printf("%d. ",i+1);
//			itemsOrdered.get(i).print();
			System.out.println(itemsOrdered.get(i).toString());
			System.out.println();
		}
		System.out.println("Total cost: " + totalCost());
		System.out.println("***************************************************");
		System.out.println();
	}
}
