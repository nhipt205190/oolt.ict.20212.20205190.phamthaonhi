package hust.soict.globalict.test.media;

import java.util.ArrayList;
import java.util.List;

import hust.soict.globalict.aims.media.Book;

public class BookTest {
	public static void main(String[] args) {
	    //book1
	    List<String> authors_b1 = new ArrayList<String>();
	    authors_b1.add("Nguyen Nhat Anh");
	    Book b1 = new Book("Toi la beto","thieu nhi", 20.00f,48,authors_b1,"xin chao xin chao cac ban minh la beto");
	    
	    
	    //book2
	    List<String> authors_b2 = new ArrayList<String>();
	    authors_b2.add("JK Rowling");
	    authors_b2.add("PTN");
	    Book b2 = new Book("Little prince","children", 23.99f,50,authors_b2,"For instance, if you come at four in the afternoon, I'll begin to be happy by three.");
	    System.out.println("--------------------------------------------");
	    
	    b1.processContent();
	    System.out.println("--------------------------------------------");
	    b2.processContent();
	    System.out.println("--------------------------------------------");
	    
	}
}
