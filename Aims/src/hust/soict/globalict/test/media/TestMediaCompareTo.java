package hust.soict.globalict.test.media;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
//import hust.soict.globalict.aims.media.MediaComparatorByCostTitle;

public class TestMediaCompareTo {
	public static void main(String[] args) {
		ArrayList<Media> collection = new ArrayList<>();
		List<Media> mediae = new ArrayList<>();
		//dvd1
	    DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King","Animation",19.95f,1,
	    		"Rogile Angle",87);
	    //dvd2
	    DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars","Sci-fi",24.95f ,2,
	    		"Gorge Lucas",124);
	    //dvd3
	    DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin","Animation", 18.99f,3,
	    		"John Musker", 90);
	    //cd1
	    CompactDisc cd1 = new CompactDisc("Folklore", "Pop", 13.99f,4,"Taylor Swift", 21);
	    //cd2
	    CompactDisc cd2 = new CompactDisc("Equals", "Pop", 15.99f,4,"Ed", 19);
	    //book1
	    List<String> authors_b1 = new ArrayList<String>();
	    authors_b1.add("Nguyen Nhat Anh");
	    Book b1 = new Book("Toi la beto","thieu nhi", 20.00f,48,authors_b1,"xin chao xin chao cac ban minh la beto");
	    
	    
	    //book2
	    List<String> authors_b2 = new ArrayList<String>();
	    authors_b2.add("JK Rowling");
	    authors_b2.add("PTN");
	    Book b2 = new Book("Little prince","children", 23.99f,authors_b2);
	    
	    // add to collection
	    collection.add((Media)dvd1);
	    collection.add((Media)dvd3);
	    collection.add((Media)dvd2);
	    collection.add((Media)cd2);
	    collection.add((Media)cd1);
	    collection.add((Media)b1);
	    collection.add((Media)b2);
	    
	    java.util.Iterator<Media> iterator = collection.iterator();
	    
	    System.out.println("--------------------------------------------");
	    System.out.println("The DVDs are currently in the order are: ");
	    
	    while(iterator.hasNext()) {
	    	System.out.println(((Media)iterator.next()).getTitle());
	    }
	    
	    //sort by cost then title
	    Collections.sort(collection,Media.COMPARE_BY_COST_TITLE);
	    
	    // iterate
	    iterator = collection.iterator();
	    
	    System.out.println("--------------------------------------------");
	    System.out.println("The DVDs in sorted order are: ");
	    Media point;
	    while(iterator.hasNext()) {
	    	point = (Media)iterator.next();
	    	System.out.println(point.getTitle() + "\t- Cost: " + point.getCost());
	    }
	    
//	    sort by title then cost
	    Collections.sort(collection,Media.COMPARE_BY_TITLE_COST);
	    
	    // iterate
	    iterator = collection.iterator();
	    
	    System.out.println("--------------------------------------------");
	    System.out.println("The DVDs in sorted order are: ");
	    while(iterator.hasNext()) {
	    	point = (Media)iterator.next();
	    	System.out.println(point.getTitle() + "\t- Cost: " + point.getCost());
	    }
	    
	    //use toString
	    mediae.add(cd1);
	    mediae.add(dvd1);
	    mediae.add(b1);
	    
	    System.out.println("--------------------------------------------");
	    for(Media m: mediae) {
	    	System.out.println(m.toString());
	    }
	    System.out.println("--------------------------------------------");
	    
	    b1.processContent();
	    
	}

    
  
}
