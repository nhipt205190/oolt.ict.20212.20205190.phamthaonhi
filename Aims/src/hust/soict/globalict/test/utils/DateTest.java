package hust.soict.globalict.test.utils;

import hust.soict.globalict.aims.utils.DateUtil;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest {
	public static void main(String[] args) {
		MyDate ob = new MyDate("June 03 2030");
		MyDate ob2 = new MyDate();
//		MyDate ob3 = new MyDate("March","3rd", "2002");
		MyDate ob4 = new MyDate("April 08 2022");
//		ob3.printN();
		ob.print();
		ob2.print();
		DateUtil.compareResult(ob,ob2);
		
		System.out.println("Sorted date: ");
		DateUtil.sortDate(ob2,ob,ob4);
	}
}
