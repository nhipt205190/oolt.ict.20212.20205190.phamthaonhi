package hust.soict.globalict.lab02;

import java.util.Arrays;
import java.util.Scanner;

public class sort {
    void increasingSort(int[] arr){
        for(int i =0; i< arr.length - 1; i++){
            for(int j =i+1; j < arr.length;j++){
                if (arr[i] > arr[j]){
                    int tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
    }
    void sum(int[] arr){
        int sum = 0;
        double avg =0;
        for(int i=0; i<arr.length;i++){
            sum = sum + arr[i];
        }
        avg = sum/arr.length;
        System.out.printf("Sum = %d\n",sum);
        System.out.printf("Average = %.2f\n",avg);

    }
    static void printArray(int[] arr){
        for(int i =0; i<arr.length; i++){
            System.out.printf("%d ", arr[i]);
        }
        System.out.println(" ");
    }
    public static void main(String[] args){
//        int myarr[] = {11,13,9,8,4,3};
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.print("Enter the size of array: ");
        n = sc.nextInt();
        int[] myarr = new int[n];
        for(int i=0; i<n; i++){
            myarr[i] = sc.nextInt();
        }
        sc.close();
        System.out.println("Input array: " + Arrays.toString(myarr));
        sort ob = new sort();
        ob.increasingSort(myarr);
        System.out.println("Sorted array: " + Arrays.toString(myarr));
        ob.sum(myarr);
    }
}
