package hust.soict.globalict.lab02;
import java.util.Scanner;

public class Matrix {
    int r,c;
    Matrix(int r, int c){
        this.r = r;
        this.c = c;
    }
    public void printMatrix(int[][] m ){
        int l1 = m.length; //row
        int l2 = m[0].length; // column
        for(int i=0; i<l1; i++){
            for(int j=0; j <l2; j++){
                System.out.printf("%d ", m[i][j]);
            }
            System.out.println();
        }
    }

    public void inputMatrix(int[][] m){
        Scanner sc = new Scanner(System.in);
        for(int i=0; i<r; i++){
            for(int j=0; j <c; j++){
                System.out.printf("m[%d][%d] = ",i,j);
                m[i][j] = sc.nextInt();
            }
        }
        sc.close();
    }
    int[][] add2matrix(int[][] m1,int[][] m2){
        int[][] sum = new int[r][c];
        for(int i =0; i<r; i++){
            for(int j=0; j<c;j++){
                sum[i][j]  = m1[i][j] + m2[i][j];
            }
        }
        return sum;
    }
    public static void main(String[] args){
        int r,c;
        Scanner sc = new Scanner(System.in);
        System.out.printf("Number of rows: ");
        r = sc.nextInt();
        System.out.printf("Number of columns: ");
        c = sc.nextInt();
        int matrix1 [][] = new int[r][c];
        int matrix2 [][] = new int[r][c];
        sc.close();
        Matrix ob = new Matrix(r,c);
        System.out.println("Input elements of matrix 1");
        ob.inputMatrix(matrix1);
        System.out.println("Input elements of matrix 2");
        ob.inputMatrix(matrix2);
        System.out.println("Matrix 1 ");
        ob.printMatrix(matrix1);
        System.out.println("Matrix 2 ");
        ob.printMatrix(matrix2);
        System.out.println("Sum of 2 matrices: ");
        ob.printMatrix(ob.add2matrix(matrix1,matrix2));
    }
}
