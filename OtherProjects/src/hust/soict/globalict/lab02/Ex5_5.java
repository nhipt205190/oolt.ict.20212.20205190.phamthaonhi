package hust.soict.globalict.lab02;
import java.util.Scanner;
class numOfDay {
    int year;
    String month;
    String monthList[][] = {{"January", "Jan", "Jan", "1"},
            {"February", "Feb.", "Feb", "2"},
            {"March", "Mar.", "Mar", "3"},
            {"April", "Apr.", "Apr", "4"},
            {"May", "May.", "May", "5"},
            {"June", "June", "Jun", "6"},
            {"July", "July", "Jul", "7"},
            {"August", "Aug.", "Aug", "8"},
            {"September", "Sept.", "Sep", "9"},
            {"October", "Oct.", "Oct", "10"},
            {"November", "Nov.", "Nov", "11"},
            {"December", "Dec.", "Dec", "12"}};

    numOfDay(int year, String month) {
        this.year = year;
        this.month = month;
    }

    int isLeap(int year) {
        if ((year % 4) == 0 && (year % 100) != 0) {
            return 1;
        } else if ((year % 400) == 0) {
            return 1;
        } else return 0;
    }

    int monthInNum(String month, String monthList[][]) {
        int key;
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 4; j++) {
                if (month.equals(monthList[i][j]) == true) {
                    key = Integer.parseInt(monthList[i][3]);
                    return key;
                }
            }
        }
        return 0;
    }
    void countDay(){
        int days = monthInNum(this.month,monthList);
        if(days != 0){
            System.out.print("Number of days: ");
            if(days == 1)
                System.out.print("31");
            else if(days == 2){
                if(isLeap(this.year) == 1)
                    System.out.print("29");
                else System.out.print("28");
            }
            else if(days == 3)
                System.out.print("31");
            else if (days == 4)
                System.out.print("30");
            else if(days == 5)
                System.out.print("31");
            else if(days == 6)
                System.out.print("30");
            else if(days == 7)
                System.out.print("31");
            else if(days == 8)
                System.out.print("31");
            else if(days == 9)
                System.out.print("30");
            else if(days == 10)
                System.out.print("31");
            else if(days == 11)
                System.out.print("30");
            else if (days == 12)
                System.out.print("31");
        }
        else
        {
            System.out.println("Invalid month!");
        }
    }
}
public class Ex5_5 {
    public static void main(String args[]){
        int year;
        String month;

        Scanner a = new Scanner(System.in);
        System.out.println("Enter year: ");
        year = a.nextInt();
        a.nextLine();
        System.out.println("Enter month:");
        month = a.next();
        a.close();
        numOfDay check = new numOfDay(year,month);
        check.countDay();
        System.exit(0);
    }
}

