package hust.soict.globalict.garbage;

import java.io.IOException;

public class NoGarbage {
	public static void main(String args[]) throws IOException{
		//improvement of using + operator to concatenate string
		final int ITERATION = 1000000;
		long startTime = System.nanoTime();
//		StringBuilder to concate two String
		StringBuilder builder = new StringBuilder();
		startTime = System.nanoTime();
		for(int i=0; i < ITERATION; i++) {
			builder.append(i);
		}
		long duration = (System.nanoTime()-startTime)/1000;
		System.out.println("Time taken to cancatenate 100000 Strings using StringBuilder (in micro) : " + duration);

		
	}
}
