package hust.soict.globalict.garbage;

import java.io.IOException;

public class Demo{
	public static void main(String args[]) throws IOException{
		final int ITERATION = 10000000;
		String s ="";
		
		//String concatenation using + operator
		long startTime = System.nanoTime();
		for(int i=0; i< ITERATION; i++) {
			s = s + Integer.toString(i);
		}
		long duration = (System.nanoTime()-startTime)/1000;
		System.out.println("Time taken to cancatenate 100000 Strings using + operator (in micro) : " + duration);

//		StringBuffer to concate String
		StringBuffer buffer = new StringBuffer();
		startTime = System.nanoTime();
		for(int i=0; i < ITERATION; i++) {
			buffer.append(i);
		}
		duration = (System.nanoTime()-startTime)/1000;
		System.out.println("Time taken to cancatenate 100000 Strings using StringBuffer (in micro) : " + duration);
		
//		StringBuilder to concate two String
		StringBuilder builder = new StringBuilder();
		startTime = System.nanoTime();
		for(int i=0; i < ITERATION; i++) {
			builder.append(i);
		}
		duration = (System.nanoTime()-startTime)/1000;
		System.out.println("Time taken to cancatenate 100000 Strings using StringBuilder (in micro) : " + duration);
	}
}
