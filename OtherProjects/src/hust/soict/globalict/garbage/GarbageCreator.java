package hust.soict.globalict.garbage;

import java.io.IOException;

public class GarbageCreator {
	public static void main(String args[]) throws IOException{
		//system cannot handle with a large number of string like this!!!
		final int ITERATION = 1000000;
		String s ="";
		
		//String concatenation using + operator
		long startTime = System.nanoTime();
		for(int i=0; i< ITERATION; i++) {
			s = s + Integer.toString(i);
		}
		long duration = (System.nanoTime()-startTime)/1000;
		System.out.println("Time taken to cancatenate 100000 Strings using + operator (in micro) : " + duration);
	}
}
