package hust.soict.globalict.lab01;

import java.lang.Math;

import javax.swing.JOptionPane;

class Equation {
    String result;
void pt1_1(double a, double b){
    double ans = -b/a;
    result = "The solution is " + ans;
    JOptionPane.showMessageDialog(null, result);
    //System.out.println("The solution is " + ans);
}

void pt1_2(double a11, double a12, double b1, double a21, double a22, double b2){
    double d = a11*a22 - a21*a12;
    double d1 = b1*a22 - b2*a12;
    double d2 = a11*b2 - a21*b1;

    if(d != 0){
        // System.out.printf("The system has a unique solution (x1,x2) = (%.2f, %.2f)\n",d1/d, d2/d);
        result = "The system has a unique solution (x1,x2) = (" + d1/d +"," + d2/d +")";
        JOptionPane.showMessageDialog(null, result);

    } else {
        if(d2 ==0 && d1 == 0){
            //System.out.println("The system has infinitely many solutions\n");
            result = "The system has infinitely many solutions\n";
            JOptionPane.showMessageDialog(null, result);
        }
        else{
            // System.out.println("The system has no solution\n");
            result = "The system has no solution\n";
            JOptionPane.showMessageDialog(null, result);
        }
    }
}
void pt2_2(double a, double b, double c){
    double delta = b*b - 4*a*c;
    if(delta >0){
/*             System.out.printf("The system has two distinct roots %.4f and %.4f\n", 
            (-b + Math.sqrt(delta))/(2*a),
            (-b - Math.sqrt(delta))/(2*a) ); */
        result = "The system has two distinct roots " + (-b + Math.sqrt(delta))/(2*a) + " and " + (-b - Math.sqrt(delta))/(2*a);
        JOptionPane.showMessageDialog(null, result);
    }
    else if(delta == 0){
        // System.out.println("The system has double root " + (-b/(2*a)));
        result = "The system has double root " + (-b/(2*a));
        JOptionPane.showMessageDialog(null, result);
    }
    else {
        // System.out.println("The system has no solution\n");
        result = "The system has no solution\n";
        JOptionPane.showMessageDialog(null, result);
    }
}
}

public class Ex2_2_6 {
public static void main(String[] args) {
    Equation solve = new Equation();
    String str1;
    int choice;
    String a,b,c,d,e,f;
    int a1,a2,a3,a4,a5,a6;
    
    str1 = JOptionPane.showInputDialog(null, "1. Linear equation.\n2. Linear system.\n3. Second-degree equation.\nChoose type of euqation: \n", JOptionPane.INFORMATION_MESSAGE);
    choice = Integer.parseInt(str1);
    switch (choice) {
        case 1:
            //ax +b = 0
            a = JOptionPane.showInputDialog("a = ");
            b = JOptionPane.showInputDialog("b = ");
            a1 = Integer.parseInt(a);
            a2 = Integer.parseInt(b);
            solve.pt1_1(a1,a2);
            break;
        case 2:
            a = JOptionPane.showInputDialog("a = ");
            b = JOptionPane.showInputDialog("b = ");
            c = JOptionPane.showInputDialog("c = ");
            a1 = Integer.parseInt(a);
            a2 = Integer.parseInt(b);
            a3 = Integer.parseInt(c);
            solve.pt2_2(a1, a2, a3);
            break;
        case 3: 
            a = JOptionPane.showInputDialog("a11 = ");
            b = JOptionPane.showInputDialog("a12 = ");
            c = JOptionPane.showInputDialog("b1 = ");
            d = JOptionPane.showInputDialog("a21 = ");
            e = JOptionPane.showInputDialog("a22 = ");
            f = JOptionPane.showInputDialog("b2 = ");
            a1 = Integer.parseInt(a);
            a2 = Integer.parseInt(b);
            a3 = Integer.parseInt(c);
            a4 = Integer.parseInt(d);
            a5 = Integer.parseInt(e);
            a6 = Integer.parseInt(f);
            solve.pt1_2(a1, a2, a3, a4, a5, a6);
            break;
    }
}
}