package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;
public class Ex2_2_5 {
    public static void main(String[] args){
        String strNum1, strNum2;
        // String strNotification = "You've just entered: ";
        String strNotification = "Results: \n";
        double sum, product, diff, avg, quotient, distance;
        double num1, num2;
        strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number",
            JOptionPane.INFORMATION_MESSAGE);
        strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number",
        JOptionPane.INFORMATION_MESSAGE);

        num1 = Double.parseDouble(strNum1);
        num2 = Double.parseDouble(strNum2);

        sum = num1 + num2;
        strNotification += "The sum of " + strNum1 + " and " + strNum2 
        + " is " + sum;

        product = num1 * num2;
        strNotification += "\nThe product of " + strNum1 + " and " + strNum2 
        + " is " + product;

        diff = num1-num2;
        strNotification += "\nThe difference of " + strNum1 + " and " + strNum2 
        + " is " + diff;
        avg = (num1 + num2) / 2;
        strNotification += "\nThe average of " + strNum1 + " and " + strNum2 
        + " is " + avg;
        quotient = num1 / num2;
        strNotification += "\nThe quotient of " + strNum1 + " and " + strNum2 
        + " is " + quotient;

        distance = Math.abs(num1-num2);
        strNotification += "\nThe distance between " + strNum1 + " and " + strNum2 
        + " is " + distance; 
        JOptionPane.showMessageDialog(null, strNotification, "Show two numbers", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
}